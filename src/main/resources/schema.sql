create table address
(
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	zip VARCHAR(10),
	city VARCHAR(20) NOT NULL
);

create table contact
(
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(20) NOT NULL,
	surname VARCHAR(20) NOT NULL,
	dob DATE, 
	address INTEGER NOT NULL,
	FOREIGN KEY (address) REFERENCES address(id)	
);