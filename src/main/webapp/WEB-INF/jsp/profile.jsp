<%@ include file="include/head.jsp" %>
<section id="admin-dashboard">
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-5"><img alt="Profile image" id="profile-img" class="rounded-circle" /> Profile</h1>
            <hr class="my-4">
            <div class="row">
                <div class="col-md-2 mt-3">
                    <ul class="nav flex-column nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#basic-info">Postavke</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#transakcije">Transakcije</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <div class="alert alert-dismissible d-none" id="admin-response"></div>

                    <div class="tab-content mt-3">
                        <div class="tab-pane fade active show" id="basic-info">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#about-you">
                                        <span class="fas fa-user"></span> About you
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#credit-card">
                                        <span class="fas fa-credit-card"></span> Credit card
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toogle="tab" href="#password-security">
                                        <span class="fas fa-lock"></span> Password & security
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toogle="tab" href="#password-security">
                                        <span class="fas fa-euro-sign"></span> Currency
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toogle="tab" href="#social-networks">
                                        <span class="fab fa-facebook"></span> Social Networks
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content bg-white border border-light border-top-0 p-4">
                                <div class="bg-light" id="about-you">
                                    <div class="p-3 bg-light border-bottom border-white">
                                        <h4>Your e-Bus Account</h4>
                                        <p class="small text-dark">These details are displayed next to your publicly shared reviews, ratings, photos, etc. Any updates you make here will also appear in past contributions.</p>
                                    </div>
                                    <div class="ml-2 mr-2 p-2">
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="imageURL">Slika profila</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <input type="file" class="form-control-file" id="imageURL" name="imageURL" />
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="username">Username</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <input type="text" class="form-control" id="username" name="username" />
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="name">Ime i prezime</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <input type="text" class="form-control" id="name" name="name" />
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="email">Email</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <input type="email" class="form-control" id="email" name="email" />
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="gender">Gender</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <select id="gender" name="gender" class="form-control custom-select">
                                                    <option value="NULL" selected>Please select gender</option>
                                                    <option value="M">Mr</option>
                                                    <option value="F">Ms</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="birthdate">Birth Date</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <input type="date" class="form-control" id="birthdate" name="birthdate" />
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="country">Drzava</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <select id="country" name="country"
                                                        class="form-control custom-select">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="city">Grad</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <select id="city" name="city" class="form-control custom-select">
                                                    <option value="1" selected>Sarajevo</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="adresa">Adresa</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <input type="text" class="form-control" id="adresa" name="adresa" />
                                            </div>
                                        </div>
                                        <div class="d-md-flex p-2 small">
                                            <div class="col-md-3 p-0 text-info">
                                                <label for="mobile">Mobile Number</label>
                                            </div>
                                            <div class="col-md-5 p-0">
                                                <input type="text" class="form-control" id="mobile" name="mobile" />
                                            </div>
                                        </div>
                                        <div class="p-2 small">
                                            <div class="col-md-3 p-0">
                                                <button class="btn btn-primary btn-sm" id="clientInfo-saveBtn" onClick="updateUserProfile()">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="transakcije">
                            <p>Lista transakcija ce biti ispisana ovdje.</p>
                            <small>Radovi u toku...</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<%@ include file="include/footer.jsp" %>

<script src="/js/profile.js"></script>