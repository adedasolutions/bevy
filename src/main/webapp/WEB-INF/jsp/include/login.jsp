<!-- Modal -->
<div class="modal fade" id="loginModal" role="dialog">
	<div class="modal-dialog">
   
      	<!-- Modal content-->
      	<div class="modal-content text-center border border-light">
        	<div class="modal-header" style="padding:35px 50px;">
           		<p class="h4"><i class="fas fa-sign-in-alt"></i> Sign in</p>
          		<button type="button" class="close" data-dismiss="modal">&times;</button>         
        	</div>
        	<div class="modal-body" style="padding:40px 50px;">
				<div class="alert alert-dismissible d-none" id="login-error"></div>
           		<div class="form-group">
<%--           			<label for="email"><span class="glyphicon glyphicon-user"></span> Username / Email</label>--%>
           			<input type="text" class="form-control" name="usernameOrEmail" placeholder="Username or E-mail" required>
       			</div>
           		<div class="form-group">
<%--           			<label for="password"><span class="glyphicon glyphicon-eye-open"></span> Password</label>--%>
           			<input type="password" class="form-control" name="password" placeholder="Password" required>
           		</div>
				<div class="d-flex justify-content-around">
					<div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
							<label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
						</div>
					</div>
					<div>
						<a href="">Forgot password?</a>
					</div>
				</div>
				<button type="button" class="btn btn-outline-primary rounded-pill btn-block my-4" id="loginBtn" onclick="login()"><i class="fas fa-sign-in-alt"></i> Prijava</button>

				<p>Not a member?
					<a href="">Register</a>
				</p>

				<p>or sign in with:</p>

				<div class="d-flex justify-content-between">
					<button id="fbLoginButton" name="fbLoginButton" class="btn btn-primary"><i class="fab fa-facebook"></i> Sign in with Facebook</button>

					<div class="d-none d-md-block"><div class="g-signin2" data-onsuccess="onSignIn" data-longtitle="true"></div></div>
					<div class="d-block d-md-none"><div class="g-signin2" data-onsuccess="onSignIn"></div></div>
				</div>
			</div>
      	</div>
	</div>
</div>
<!-- ---------------------------------------------------------------------------------- -->
<!-- Custom Javascript -->
<script src="/js/login.js"></script>
<!-- ---------------------------------------------------------------------------------- -->