<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!-- Modal -->
<div class="modal fade" id="registerModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content text-center">
			<div class="modal-header" style="padding: 35px 50px;">
				<h4 class="font-weight-bold"><i class="fas fa-user-plus"></i> Registracija</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" style="padding: 40px 50px;">
				<div class="alert alert-dismissible d-none" id="register-error"></div>

				<div class="form-row mb-4">
					<div class="col">
						<input type="text" class="form-control" name="username" placeholder="Username" required>
					</div>
					<div class="col">
						<input type="text" class="form-control" name="name" placeholder="First and last name" required>
					</div>
				</div>
				<input type="email" class="form-control mb-4" name="email" placeholder="E-mail" required>

				<input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="password-hint" required>
				<small id="password-hint" class="form-text text-muted mb-4">
					At least 8 characters and 1 digit
				</small>

				<button type="button" onclick="register()" class="btn btn-outline-primary rounded-pill my-4 btn-block">
					<i class="fas fa-user-plus"></i> Sign in
				</button>

				<p>or sign in with:</p>

				<div class="d-flex justify-content-between">
					<button id="fbLoginButton" name="fbLoginButton" class="btn btn-primary"><i class="fab fa-facebook"></i> Sign in with Facebook</button>
					<div class="g-signin2" data-onsuccess="onSignIn" data-longtitle="true"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ---------------------------------------------------------------------------------- -->
<!-- Custom Javascript -->
<script src="/js/login.js"></script>
<script src="/js/register.js"></script>
<!-- ---------------------------------------------------------------------------------- -->