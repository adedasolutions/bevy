<ul class="nav nav-tabs nav-justified flex-column flex-sm-row " id="search-tabs" role="tablist">

	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#gradske" role="tab" aria-controls="gradske" aria-selected="true">Prigradske i gradske linije</a>
	</li>
		<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#federalne" role="tab" aria-controls="federalne" aria-selected="false">Federalne i drzavne linije</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#medjunarodne" role="tab" aria-controls="medjunarodne" aria-selected="false">Medjunarodne linije</a>
	</li>
</ul>
<div class="tab-content border-bottom border-left border-right border-white " id="search-form">
	<div class="tab-pane fade show active" id="gradske" role="tabpanel" aria-labelledby="gradske-tab">
		<div class="bg-info p-3 pt-4">
			<div class="alert alert-dismissible d-none" id="search-response"></div>
			<div class="d-lg-flex flex-wrap justify-content-between">
				<div class="col-12 col-lg-8">
					<div class="d-flex mb-4">
						<label for="polazisteGradske" class="d-flex align-self-center pr-2 text-white h4"><i class="fas fa-map-marker-alt text-danger ml-2"></i></label>
						<input type="text" class="form-control form-control-lg input-autocomplete" name="polazisteGradske" id="polazisteGradske" placeholder="Polaziste" required>
					</div>
					<div class="d-flex mb-4">
						<label for="odredisteGradske" class="d-flex align-self-center pr-2 text-white h4"><i class="fas fa-map-marker-alt text-danger ml-2"></i></label>
						<input type="text" class="form-control form-control-lg input-autocomplete" name="odredisteGradske" id="odredisteGradske" placeholder="Odrediste" required>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="">
						<div class="d-flex mb-4">
							<label for="mjestoGradske" class="d-flex align-self-center pr-2 text-white h5"><i class="fas fa-city text-danger"></i></label>
							<select class="custom-select custom-select-lg" name="mjestoGradske" id="mjestoGradske">
								<option value="ZENICA" selected>Zenica</option>
								<option value="SARAJEVO">Sarajevo</option>
							</select>
						</div>
						<div class="d-flex mb-4">
							<label for="polazakGradske" class="d-flex align-self-center pr-3 text-white h5"><i class="fas fa-calendar-alt"></i></label>
							<input class="form-control" name="polazakGradske" id="polazakGradske" placeholder="Polazak" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-12">
				<button class="btn-lg btn-warning btn-block" id="search-btn" onclick="search(3)"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</div>
	<div class="tab-pane fade" id="federalne" role="tabpanel" aria-labelledby="federalne-tab">
		<div class="bg-info p-3 pt-4 ">
			<div class="alert alert-dismissible d-none" id="search-response"></div>
			<div class="d-lg-flex flex-wrap justify-content-between">
				<div class="col-12 col-lg-8">
					<div class="d-flex mb-4">
						<label for="polazisteFederalne" class="d-flex align-self-center pr-2 text-white h4"><i class="fas fa-map-marker-alt text-danger ml-2"></i></label>
						<input type="text" class="form-control form-control-lg" name="polazisteFederalne" id="polazisteFederalne" placeholder="Polaziste" required>
					</div>
					<div class="d-flex mb-4">
						<label for="odredisteFederalne" class="d-flex align-self-center pr-2 text-white h4"><i class="fas fa-map-marker-alt text-danger ml-2"></i></label>
						<input type="text" class="form-control form-control-lg" name="odredisteFederalne" id="odredisteFederalne" placeholder="Odrediste" required>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="my-4">
						<div class="d-flex py-3">
							<label for="polazakFederalne" class="d-flex align-self-center pr-3 text-white h5"><i class="fas fa-calendar-alt"></i></label>
							<input class="form-control" name="polazakFederalne" id="polazakFederalne" placeholder="Polazak" />
						</div>
					</div>
				</div>
			</div>
			<div class="d-lg-flex flex-wrap justify-content-between">
				<div class="col-12 col-lg-8 mb-3 mb-lg-0">
					<div class="dropdown">
						<button class="btn btn-lg btn-light btn-block dropdown-toggle d-flex justify-content-around align-items-center" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    	<span class="small align-self-center">
					    		<i class="fas fa-user"></i>: x<span id="odrasli-federalne">1</span>
					    	</span>
					    	<div>|</div>
					    	<span class="small align-self-center">
					    		<i class="fas fa-male"></i>: x<span id="djeca-12-federalne">0</span>
					    	</span>
					    	<div>|</div>
					    	<span class="small align-self-center">
					    		<i class="fas fa-child"></i>: x<span id="djeca-6-federalne">0</span>
					    	</span>
					    	<div>|</div>
					    	<span class="small align-self-center">
					    		<i class="fas fa-baby"></i>: x<span id="djeca-2-federalne">0</span>
					    	</span>
					    	<div>|</div>
					    	<span class="small align-self-center">
					    		<i class="fas fa-bicycle"></i>: x<span id="bicikli-federalne">0</span>
					    	</span>
					  	</button>
					  	<div class="dropdown-menu col-12 col-md-7" aria-labelledby="putnici">
					  		<div class="p-3">
						    	<div class="d-flex justify-content-between">
						    		<label for="odrasliFederalne" class="text-dark align-self-center"><i class="fas fa-user"></i> Odrasli: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="odrasliFederalne" id="odrasliFederalne" min="1" value="1" />
						    	</div>
						    	<div class="dropdown-divider"></div>
						    	<div class="d-flex justify-content-between">
						    		<label for="djeca12Federalne" class="text-dark align-self-center"><i class="fas fa-male"></i> Djeca do 12 godina: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="djeca12Federalne" id="djeca12Federalne" min="0" value="0" />
						    	</div>
						    	<div class="dropdown-divider"></div>
						    	<div class="d-flex justify-content-between">
						    		<label for="djeca6Federalne" class="text-dark align-self-center"><i class="fas fa-child"></i> Djeca do 6 godina: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="djeca6Federalne" id="djeca6Federalne" min="0" value="0" />
						    	</div>
						    	<div class="dropdown-divider"></div>
						    	<div class="d-flex justify-content-between">
						    		<label for="djeca2Federalne" class="text-dark align-self-center"><i class="fas fa-baby"></i> Djeca do 2 godine: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="djeca2Federalne" id="djeca2Federalne" min="0" value="0" />
						    	</div>
						    	<div class="dropdown-divider"></div>
						    	<div class="d-flex justify-content-between">
						    		<label for="bicikliFederalne" class="text-dark align-self-center"><i class="fas fa-bicycle"></i> Bicikli: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="bicikliFederalne" id="bicikliFederalne" min="0" value="0" />
						    	</div>
					    	</div>
					  	</div>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<button class="btn-lg btn-warning btn-block" id="searchFederalne" onclick="search(2)"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane fade" id="medjunarodne" role="tabpanel" aria-labelledby="medjunarodne-tab">
		<div class="bg-info p-3 pt-4 ">
			<div class="alert alert-dismissible d-none" id="search-response"></div>
			<div class="d-lg-flex flex-wrap justify-content-between">
				<div class="col-12 col-lg-8">
					<div class="d-flex mb-4">
						<label for="polazisteMedjunarodne" class="d-flex align-self-center pr-2 text-white h4"><i class="fas fa-map-marker-alt text-danger ml-2"></i></label>
						<input type="text" class="form-control form-control-lg" name="polazisteMedjunarodne" id="polazisteMedjunarodne" placeholder="Polaziste" required>
					</div>
					<div class="d-flex mb-4">
						<label for="odredisteMedjunarodne" class="d-flex align-self-center pr-2 text-white h4"><i class="fas fa-map-marker-alt text-danger ml-2"></i></label>
						<input type="text" class="form-control form-control-lg" name="odredisteMedjunarodne" id="odredisteMedjunarodne" placeholder="Odrediste" required>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="my-4">
						<div class="d-flex py-3">
							<label for="polazakMedjunarodne" class="d-flex align-self-center pr-3 text-white h5"><i class="fas fa-calendar-alt"></i></label>
							<input class="form-control" name="polazakMedjunarodne" id="polazakMedjunarodne" placeholder="Polazak" />
						</div>
					</div>
				</div>
			</div>
			<div class="d-lg-flex flex-wrap justify-content-between">
				<div class="col-12 col-lg-8 mb-3 mb-lg-0">
					<div class="dropdown">
						<button class="btn btn-lg btn-light btn-block dropdown-toggle d-flex justify-content-around align-items-center" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    	<span class="small align-self-center">
					    		<i class="fas fa-user"></i>: x<span id="odrasli-medjunarodne">1</span>
					    	</span>
					    	<div>|</div>
					    	<span class="small align-self-center">
					    		<i class="fas fa-male"></i>: x<span id="djeca-12-medjunarodne">0</span>
					    	</span>
					    	<div>|</div>
					    	<span class="small align-self-center">
					    		<i class="fas fa-child"></i>: x<span id="djeca-6-medjunarodne">0</span>
					    	</span>
					    	<div>|</div>
					    	<span class="small align-self-center">
					    		<i class="fas fa-baby"></i>: x<span id="djeca-2-medjunarodne">0</span>
					    	</span>
					    	<div>|</div>
					    	<span class="small align-self-center">
					    		<i class="fas fa-bicycle"></i>: x<span id="bicikli-medjunarodne">0</span>
					    	</span>
					  	</button>
					  	<div class="dropdown-menu col-12 col-md-7" aria-labelledby="putniciMedjunarodne">
					  		<div class="p-3">
						    	<div class="d-flex justify-content-between">
						    		<label for="odrasliMedjunarodne" class="text-dark align-self-center"><i class="fas fa-user"></i> Odrasli: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="odrasliMedjunarodne" id="odrasliMedjunarodne" min="1" value="1" />
						    	</div>
						    	<div class="dropdown-divider"></div>
						    	<div class="d-flex justify-content-between">
						    		<label for="djeca12Medjunarodne" class="text-dark align-self-center"><i class="fas fa-male"></i> Djeca do 12 godina: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="djeca12Medjunarodne" id="djeca12Medjunarodne" min="0" value="0" />
						    	</div>
						    	<div class="dropdown-divider"></div>
						    	<div class="d-flex justify-content-between">
						    		<label for="djeca6Medjunarodne" class="text-dark align-self-center"><i class="fas fa-child"></i> Djeca do 6 godina: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="djeca6Medjunarodne" id="djeca6Medjunarodne" min="0" value="0" />
						    	</div>
						    	<div class="dropdown-divider"></div>
						    	<div class="d-flex justify-content-between">
						    		<label for="djeca2Medjunarodne" class="text-dark align-self-center"><i class="fas fa-baby"></i> Djeca do 2 godine: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="djeca2Medjunarodne" id="djeca2Medjunarodne" min="0" value="0" />
						    	</div>
						    	<div class="dropdown-divider"></div>
						    	<div class="d-flex justify-content-between">
						    		<label for="bicikliMedjunarodne" class="text-dark align-self-center"><i class="fas fa-bicycle"></i> Bicikli: </label>
						    		<input type="number" class="form-control align-self-center col-3" name="bicikliMedjunarodne" id="bicikliMedjunarodne" min="0" value="0" />
						    	</div>
					    	</div>
					  	</div>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<button class="btn-lg btn-warning btn-block" id="searchMedjunarodne" onclick="search(1)"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- ---------------------------------------------------------------------------------- -->
<!-- Custom Javascript -->
<script src="/js/search-bar.js"></script>
<!-- ---------------------------------------------------------------------------------- -->