<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="google-signin-client_id" content="323058167373-u7nfnp0s608fdm7m1g6nehelrqs9jps2.apps.googleusercontent.com">
		
	    <title>eBus</title>
	    
<!-- 	    <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.css"> -->
		<link rel="stylesheet" href="/webjars/bootstrap/4.3.1/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/vendor/easy-autocomplete/css/easy-autocomplete.css">
		<link rel="stylesheet" href="/vendor/datepicker/css/datepicker.css">
		<link rel="stylesheet" href="/css/custom.css" />
		
	    
<!-- 	    <script src="/vendor/jquery/jquery-3.4.1.min.js"></script> -->
		<script src="/webjars/jquery/3.4.1/jquery.min.js"></script>
    	<script src="/vendor/popper/js/popper.min.js"></script>
<!--     	<script src="/vendor/bootstrap/js/bootstrap.min.js"></script> -->
		<script src="/webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<script src="/vendor/fontawesome/js/all.js"></script>
    	<script src="/vendor/easy-autocomplete/js/jquery.easy-autocomplete.js"></script>
    	<script src="/vendor/datepicker/js/datepicker.min.js"></script>
    	<script src="/vendor/datepicker/js/datepicker.en.js"></script>
    	<script src="//js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js"></script>
    	
    	<!-- DataTable -->
    	<link rel="stylesheet" type="text/css" href="/vendor/datatable/DataTables-1.10.18/css/jquery.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/AutoFill-2.3.3/css/autoFill.dataTables.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/Buttons-1.5.6/css/buttons.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/ColReorder-1.5.0/css/colReorder.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/FixedColumns-3.2.5/css/fixedColumns.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/FixedHeader-3.1.4/css/fixedHeader.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/KeyTable-2.5.0/css/keyTable.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/Responsive-2.2.2/css/responsive.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/RowGroup-1.1.0/css/rowGroup.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/RowReorder-1.2.4/css/rowReorder.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/Scroller-2.0.0/css/scroller.dataTables.min.css"/>
		<link rel="stylesheet" type="text/css" href="/vendor/datatable/Select-1.3.0/css/select.dataTables.min.css"/>
		 
		<script type="text/javascript" src="/vendor/datatable/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/AutoFill-2.3.3/js/dataTables.autoFill.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/Buttons-1.5.6/js/buttons.colVis.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/ColReorder-1.5.0/js/dataTables.colReorder.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/FixedColumns-3.2.5/js/dataTables.fixedColumns.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/FixedHeader-3.1.4/js/dataTables.fixedHeader.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/KeyTable-2.5.0/js/dataTables.keyTable.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/Responsive-2.2.2/js/dataTables.responsive.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/RowGroup-1.1.0/js/dataTables.rowGroup.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/RowReorder-1.2.4/js/dataTables.rowReorder.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/Scroller-2.0.0/js/dataTables.scroller.min.js"></script>
		<script type="text/javascript" src="/vendor/datatable/Select-1.3.0/js/dataTables.select.min.js"></script>
    </head>
	<body>
		<c:set var="isLoggedIn" value="false" scope="request" />

		<div class="position-absolute col-12 col-md-4 w-100 p-3 text-center font-weight-bolder rounded-left d-none" id="global-response" style="z-index: 20; right: 0; top: 53px;"></div>
		
		<nav class="navbar navbar-expand-lg navbar-light navbar-bg">
			<div class="container">
				<a class="navbar-brand" href="/">e-<img src="/img/bus.png" width="80" height="30"/></a>
			  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
			   		<span class="navbar-toggler-icon"></span>
			  	</button>
			
			  	<div class="collapse navbar-collapse" id="navbar">
			  		<ul class="navbar-nav pl-4 font-weight-bold">
			  			<li class="nav-item active ml-4">
			        		<a class="nav-link" href="#">Elektronske karte <span class="sr-only">(current)</span></a>
			      		</li>
			      		<li class="nav-item  ml-4 ">
			        		<a class="nav-link" href="#">Usluge</a>
			      		</li>
			      		<li class="nav-item ml-4">
			        		<a class="nav-link" href="#">Pomoć</a>
			      		</li>
			      		<li class="nav-item ml-4">
			        		<a class="nav-link" href="#">O nama</a>
			      		</li>
			  		</ul>
			  		
		    		<ul class="navbar-nav ml-auto" id="nav-items">
			      		
		    		</ul>
			  	</div>
			</div>
		</nav>
		
		<!-- Login Modal -->
		<%@ include file="login.jsp" %>
		
		<!-- Register Modal -->
		<%@ include file="register.jsp" %>

