<%@ include file="include/head.jsp" %>
	<section id="admin-dashboard">
		<div class="jumbotron">
			<div class="container">
				<h1 class="display-5">ADMIN CP</h1>
				<hr class="my-4">
				<div class="row">
					<div class="col-md-2 mt-3">
						<ul class="nav flex-column nav-pills">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="pill" href="#basic-info">Agencija</a>
							</li>
							<li class="nav-item">
			                    <a class="nav-link" data-toggle="pill" href="#edit-linije">Linije</a>
			                </li>
						</ul>
					</div>
					<div class="col-md-10">
						<div class="alert alert-dismissible d-none" id="admin-response"></div>
						<div class="tab-content bg-light mt-3">
							<div class="tab-pane fade active show" id="basic-info">
								<div class="p-3">
									<h3>Informacije</h3>
								</div>
								<form action="" method="POST" enctype="multipart/form-data">
									<div class="d-md-flex p-3">
										<div class="col-md-2 align-self-center">
											<label for="nazivAgencije">Naziv agencije</label>
										</div>
										<div class="col-md-10">
											<input type="text" name="nazivAgencije" class="form-control" value="" required>
										</div>
									</div>
									<div class="d-md-flex p-3">
										<div class="col-md-2 align-self-center">
											<label for="adresa">Adresa</label>
										</div>
										<div class="col-md-10">
											<div class="custom-file">
												<input type="text" class="form-control" name="adresa" value="" required>
											</div>
										</div>
									</div>
									<div class="p-3">
										<div class="col-md-2">
											<input type="submit" class="btn btn-info" name="update-info" value="Sacuvaj promjene">
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade" id="edit-linije">
								<div class="p-3 d-flex justify-content-between border-bottom">
									<h3>Linije</h3>
									
									<div class="col-6 col-md-3">
										<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#linija-form"><i class="fas fa-plus"></i> Linija</button>
									</div>
								</div>
								<div class="modal fade" id="linija-form" tabindex="-1" role="dialog" aria-labelledby="linija-form" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
								    	<div class="modal-content">
								      		<div class="modal-header">
								        		<h5 class="modal-title">Dodaj liniju</h5>
								        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								          			<span aria-hidden="true">&times;</span>
								        		</button>
								      		</div>
								      		<div class="modal-body">
								      			<div class="alert alert-dismissible d-none" id="linija-modal-response"></div>
												<select class="custom-select" name="linijeSelect" id="linijeSelect">
													<option value="0" selected>Izaberi liniju</option>
												</select>
								      		</div>
								      		<div class="modal-footer">
								        		<input type="submit" class="btn btn-primary" onclick="dodajLiniju()" value="Dodaj">
			        							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								      		</div>
								    	</div>
								  	</div>
								</div>
								<div class="modal fade" id="delete-linija" tabindex="-1" role="dialog" aria-labelledby="delete-linija" aria-hidden="true">
									<div class="modal-dialog" role="document">
								    	<div class="modal-content">
								      		<div class="modal-header">
								        		<h5 class="modal-title"><i class="fas fa-trash"></i> <span id="deleteNazivLinije"></span></h5>
								        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								          			<span aria-hidden="true">&times;</span>
								        		</button>
								      		</div>
								      		<div class="modal-body">
								      			<h5 class="text-info">Da li ste sigurni da zelite izbrisati liniju ?</h5>
								      		</div>
								      		<div class="modal-footer">
			        							
								      		</div>
								    	</div>
								  	</div>
								</div>
								<div class="modal fade" id="delete-relacija" tabindex="-1" role="dialog" aria-labelledby="delete-linija" aria-hidden="true">
									<div class="modal-dialog" role="document">
								    	<div class="modal-content">
								      		<div class="modal-header">
								        		<h5 class="modal-title"><i class="fas fa-trash"></i> <span id="deleteNazivRelacije"></span></h5>
								        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								          			<span aria-hidden="true">&times;</span>
								        		</button>
								      		</div>
								      		<div class="modal-body">
								      			<h5 class="text-info">Da li ste sigurni da zelite izbrisati relaciju ?</h5>
								      		</div>
								      		<div class="modal-footer">
			        							
								      		</div>
								    	</div>
								  	</div>
								</div>
								<div class="modal fade" id="relacija-form" tabindex="-1" role="dialog" aria-labelledby="relacija-form" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Dodaj relaciju linije: <span id="modalTitleRelacijaZaLiniju"></span></h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="alert alert-dismissible d-none" id="relacija-modal-response"></div>
												<div class="p-3 d-md-flex justify-content-md-between pb-md-0">
													<div id="modalRelacijaHiddenInput"></div>
													<div class="col-md-6 mb-1 mb-md-0">
														<div class="d-md-flex align-items-center">
															<div class="col-md-4 p-md-0">
																<label for="polaziste">Polaziste</label>
															</div>
															<div class="col-md-8 p-md-0 pl-md-2">
																<input type="text" name="polaziste" id="polaziste-relacijaForm" class="form-control" required>
															</div>
														</div>
														<div class="d-none d-md-block align-self-center ride-station-names col-12 p-0">
															<div class="ride-station-name">
																<div class="station-name departure-station-name h6"></div>
															</div>
														</div>
													</div>
													<div class="col-md-6 mb-1 mb-md-0">
														<div class="d-md-flex align-items-center">
															<div class="col-md-4 p-md-0">
																<label for="odrediste">Odrediste</label>
															</div>
															<div class="col-md-8 p-md-0 pl-md-2">
																<input type="text" name="odrediste" id="odrediste-relacijaForm" class="form-control" required>
															</div>
														</div>
														<div class="d-none d-md-block align-self-center ride-station-names col-12 p-0">
															<div class="ride-station-name">
																<div class="station-name departure-station-name h6"></div>
															</div>
														</div>
													</div>
												</div>
												<div class="d-flex d-md-none justify-content-between">
													<div class="ride-station-names text-center">
														<div class="ride-station-name">
															<div class="station-name departure-station-name h6"></div>
														</div>
														<div class="ride-station-name">
															<div class="station-name arrival-station-name h6"></div>
														</div>
													</div>
													<div class="ride-station-names text-center">
														<div class="ride-station-name">
															<div class="station-name departure-station-name h6"></div>
														</div>
														<div class="ride-station-name">
															<div class="station-name arrival-station-name h6"></div>
														</div>
													</div>
												</div>
												<div class="p-3 d-md-flex justify-content-md-between pt-md-0">
													<div class="col-md-6 mb-1 mb-md-0">
														<div class="d-none d-md-block align-self-center ride-station-names col-4 p-0">
															<div class="ride-station-name">
																<div class="station-name arrival-station-name h6"></div>
															</div>
														</div>
														<div class="d-md-flex align-items-center">
															<div class="col-md-6 p-md-0">
																<label for="vrijemePolaska">Vrijeme polaska</label>
															</div>
															<div class="col-md-6 p-md-0 pl-md-2">
																<input type="text" name="vrijemePolaska" class="form-control">
															</div>
														</div>
													</div>
													<div class="col-md-6 mb-1 mb-md-0">
														<div class="d-none d-md-block align-self-center ride-station-names col-4 p-0">
															<div class="ride-station-name">
																<div class="station-name arrival-station-name h6"></div>
															</div>
														</div>
														<div class="d-md-flex align-items-center">
															<div class="col-md-6 p-md-0">
																<label for="vrijemeDolaska">Vrijeme dolaska</label>
															</div>
															<div class="col-md-6 p-md-0 pl-md-2">
																<input type="text" name="vrijemeDolaska" class="form-control">
															</div>
														</div>
													</div>
												</div>
												<div class="d-flex p-3">
													<div class="col-2 align-self-center">
														<label for="povratna">Povratna</label>
													</div>
													<div class="col-10 ml-3 ml-md-0">
														<label class="switch">
															<input type="checkbox" name="povratna" id="povratna" value="1" class="success">
															<span class="slider round"></span>
														</label>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<input type="submit" class="btn btn-primary" value="Dodaj">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<div class="p-3" id="linije-agencije">
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<%@ include file="include/footer.jsp" %>

<script src="/js/admin.js"></script>