<section class="bg-light" id="rides-page">
	<div class="container">
		<div class="d-lg-flex">
			<div class="col-12 col-lg-9 py-4 px-0">
				<div class="d-flex justify-content-between text-noselect small text-monospace">
					<div class="col-6 py-2">
						<select class="custom-select custom-select-sm" id="sort-by">
							<option value="0" selected>Polazak (najraniji)</option>
							<option value="1" disabled>Cijena (najniza)</option>
							<option value="2" disabled>Trajanje (najkrace)</option>
						</select>
					</div>
					<div class="col-6 py-2">
						<div class="dropdown">
							<div class="dropdown-toggle cursor-pointer btn btn-sm bg-white btn-block border rounded show-filters-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/img/filters.svg" class="align-self-center" alt="Filters" />Filtriraj prema</div>
							<div class="dropdown-menu col-12" aria-labelledby="putnici">
						  		<div class="p-3">
<!-- 								    	<div class="d-flex justify-content-between"> -->
<!-- 								    		<label for="odrasli" class="text-dark align-self-center"><i class="fas fa-user"></i> Odrasli: </label> -->
<!-- 								    		<input type="number" class="form-control align-self-center col-3" name="odrasli" id="odrasli" min="1" value="1" /> -->
<!-- 								    	</div> -->
							    	<div class="custom-control custom-checkbox">
								    	<input type="checkbox" class="custom-control-input" id="is-direct" />
								      	<label class="custom-control-label" for="is-direct">Direktna putavanja</label>
								    </div>
							    	<div class="dropdown-divider"></div>
						    	</div>
						  	</div>
						</div>
					</div>
				</div>
					<div class="py-2">
						<h1 class="text-dark text-center text-noselect" id="naziv-linije"></h1>
						<div class="px-3">
							<ul class="nav nav-tabs nav-justified small">
								<li class="nav-item">
							    	<a class="nav-link" data-toggle="tab" role="tab" aria-controls="rides-yesterday" aria-selected="false" href="#rides-yesterday" id="datum-jucer"></a>
							  	</li>
							  	<li class="nav-item">
							    	<a class="nav-link active" data-toggle="tab" role="tab" aria-controls="rides-today" aria-selected="false" href="#rides-today" id="datum-danas"></a>
							  	</li>
							  	<li class="nav-item">
							    	<a class="nav-link" data-toggle="tab" role="tab" aria-controls="rides-tomorrow" aria-selected="false" href="#rides-tomorrow" id="datum-sutra"></a>
							  	</li>
						  	</ul>
					  	</div>
						<div class="px-3">
							<div class="bg-white px-4 py-4 border border-top-0">
								<div class="d-flex text-muted text-monospace text-noselect border-bottom border-width-2">
									<div class="col-6 p-0">Polasci/dolasci</div>
									<div class="col-3 p-0">Trajanje</div>
									<div class="col-3 p-0">CIJENA</div>
								</div>
								<div class="tab-content">
									<div class="tab-pane fade show active" id="rides-today" role="tabpanel" aria-labelledby="rides-tab">
										<div id="rides"></div>
									</div>
									<div class="tab-pane fade" id="rides-tomorrow" role="tabpanel" aria-labelledby="rides-tab">
										<div id="rides"></div>
									</div>
									<div class="tab-pane fade" id="rides-yesterday" role="tabpanel" aria-labelledby="rides-tab">
										<div id="rides"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
			<div class="col-12 col-lg-3 px-0 py-4">
				<div class="py-2">
					<div class="card">
						<h3 class="card-header text-center">Korpa</h3>
						
						<div class="card-body">
							<p class="text-center text-muted">Pretrazi dostupna putovanja i dodaj ih u korpu prije kupovine.</p>
							<button class="btn btn-lg btn-block btn-success disabled">Kupi</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

