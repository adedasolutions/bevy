package com.bevy.payload;

import org.joda.time.LocalDate;


public class ContactPayload {	

	private String name;

	private String surname;
	
	private LocalDate dob;
	
	private String zip;
	
	private String city;
	
	public ContactPayload() {		
	}

	public ContactPayload(String name, String surname, LocalDate dob, String zip, String city) {
		this.name = name;
		this.surname = surname;
		this.dob = dob;
		this.zip = zip;
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String toJSONString() {
		return "{ \"name\":\"" + name 
				+ "\", \"surname\":\"" + surname 
				+ "\", \"dob\":\"" + dob 
				+ "\", \"zip\":\"" + zip
				+ "\", \"city\":\"" + city + 
				"\"}";
	}

}
