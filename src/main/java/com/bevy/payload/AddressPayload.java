package com.bevy.payload;

public class AddressPayload {
	
	private String zip;
	
	private String city;
	
	public AddressPayload() {		
	}

	public AddressPayload(String zip, String city) {
		this.zip = zip;
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String toJSONString() {
		return "{\"zip\":\"" + zip 
				+ "\", \"city\":\"" + city 
				+ "\"}";
	}
	
	
	

}
