package com.bevy.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.bevy.model.Address;
import com.bevy.model.Contact;
import com.bevy.payload.ContactPayload;
import com.bevy.payload.AddressPayload;
import com.bevy.service.AddressService;
import com.bevy.service.ContactService;
import com.bevy.util.JsonUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.util.Optional;

@RestController
public class ContactController {

	@Autowired
	ContactService contactService;

	@Autowired
	AddressService addressService;

	@PutMapping(value = "/contact", consumes = "application/json", headers = "Accept=application/json")
	public ResponseEntity<String> saveOrUpdateContact(@RequestBody ContactPayload payload)
			throws JsonParseException, JsonMappingException, IOException, ParseException {

		Address address = null;

		Optional<Address> addressExist = addressService.getAddressByZipOrCity(payload.getZip(), payload.getCity());

		if (!addressExist.isEmpty()) {
			address = addressExist.get();
		} else {
			Address toSaveLoc = new Address();
					toSaveLoc.setZip(payload.getZip());
					toSaveLoc.setCity(payload.getCity());

			address = addressService.saveAddress(toSaveLoc);
		}

		Contact contact = new Contact();
				contact.setName(payload.getName());
				contact.setSurname(payload.getSurname());
				contact.setDob(payload.getDob());
				contact.setAddress(address.toJSONString());

		contactService.saveContact(contact);

		return new ResponseEntity<String>(contact.toJSONString(), HttpStatus.OK);
	}

	@GetMapping(value = "/contact/all", produces = "application/json")
	public ResponseEntity<String> getAllContacts() throws IOException {

		List<Contact> contacts = contactService.getAllContacts();

		return new ResponseEntity<String>(JsonUtil.getJsonFromObject(contacts), HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/contact/filter", produces = "application/json")
	public ResponseEntity<String> filterContacts(@RequestBody ContactPayload filter) throws IOException {

		List<Contact> filteredContacts = contactService.filterContacts(filter);

		return new ResponseEntity<String>(JsonUtil.getJsonFromObject(filteredContacts), HttpStatus.OK);
	}
	
	

}
