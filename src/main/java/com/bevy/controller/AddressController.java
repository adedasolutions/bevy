package com.bevy.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bevy.model.Address;
import com.bevy.service.AddressService;
import com.bevy.util.JsonUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
public class AddressController {
	
	@Autowired
	AddressService addressService;	
	
	@PutMapping(value = "/address", consumes = "application/json", headers = "Accept=application/json")
	public ResponseEntity<String> saveOrUpdateAddress(@RequestBody String address)
				throws JsonParseException, JsonMappingException, IOException, ParseException {
	
		Address resp = (Address) JsonUtil.getAddressFromJSON(address);
		Address l =	addressService.saveAddress(resp);

		return new ResponseEntity<String>(l.toJSONString(), HttpStatus.OK);
	}	
	
	@GetMapping("/location/all")
	public ResponseEntity<String>  getAllLocations() throws IOException {	
		
			List<Address> locations = addressService.getAllAddresses();		
			
		return new ResponseEntity<String>(locations.toString(), HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/location/{zip}", produces = "application/json")
	public ResponseEntity<String>  getByZip(@PathVariable String zip) throws IOException {
		
			Address address = addressService.getByZip(zip);
		
		return new ResponseEntity<String>(address.toJSONString(), HttpStatus.OK);
	}

}
