package com.bevy.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bevy.model.Contact;
import com.bevy.payload.ContactPayload;
import com.bevy.repository.ContactRepository;

@Service
public class ContactService {

	private static final Log log = LogFactory
			.getLog(ContactService.class);
	
	@Autowired
	ContactRepository contactRepository;	
	
	
	@Transactional
	public List<Contact> filterContacts(ContactPayload filter) throws IOException {
		log.info("...start filter Contacts instances");
		try {
			
			List<Contact> filteredContacts = null;
			
			boolean nameExist = false;
			boolean surnameExist = false;
			boolean dobExist = false;
			boolean zipExist = false;
			boolean cityExit = false;
			
			if(!filter.getName().isBlank() || !filter.getName().isEmpty() || !filter.getName().equals("")) {
				nameExist = true;
			}
			if(!filter.getSurname().isBlank() || !filter.getSurname().isEmpty() || !filter.getSurname().equals("")) {
				surnameExist = true;
			}
			if(!filter.getDob().equals("")) {
				dobExist = true;
			}
//			if(!filter.getZip().isBlank() || !filter.getZip().isEmpty() || !filter.getZip().equals("")) {
//				zipExist = true;
//			}
//			if(!filter.getCity().isBlank() || !filter.getCity().isEmpty() || !filter.getCity().equals("")) {
//				cityExit = true;
//			}
			
			
			if(nameExist || surnameExist) {
				filteredContacts = contactRepository.findByNameOrSurnameIgnoreCase(filter.getName(), filter.getSurname());
			} else if(dobExist) {
				filteredContacts = contactRepository.findByDob(filter.getDob());
			} 
			
//			else if(zipExist || cityExit) {
//				filteredContacts = contactRepository.findByZipOrCity(filter.getZip(), filter.getCity());
//			}
	
			if(filteredContacts.isEmpty()) {
				log.error("Contacts not found");				
			}else {
				log.info("Filter Contacts");
			}				

			return filteredContacts;
		} catch (RuntimeException e) {
			log.info("Filter Contacts failed", e);
			throw e;
		}
	}
	
	@Transactional
	public List<Contact> getAllContacts() throws IOException {
		log.info("...start GET Contacts instances");
		try {

			List<Contact> contacts = contactRepository.findAll();
	
				if(contacts.isEmpty()) {
					log.error("Contacts not found");				
				}else {
					log.info("GET All Contacts");
				}				

			return contacts;
		} catch (RuntimeException e) {
			log.info("GET Contacts failed", e);
			throw e;
		}
	}
	
	@Transactional
	public Contact getContact(Integer id) {
		log.info("...start GET contact instance");
		try {
			Contact contact = contactRepository.getOne(id);			
					contact.getName();	
			log.info("GET contact successful");
			return contact;
		} catch (RuntimeException e) {
			log.info("GET contact failed", e);
			throw e;
		}
	}
	
	@Transactional
	public Contact saveContact(Contact contact) {		
		log.info("...start saving contact instance");		
		try {	
			log.info("New contact successful saved");
			return contactRepository.saveAndFlush(contact);	
		} catch (RuntimeException e) {
			log.info("...saving contact failed", e);
			throw e;
		}
	}
	
	@Transactional
	public void deleteContact(Integer id) {		
		log.info("...start deleting contact instance");		
		try {
			contactRepository.deleteById(id);		
			log.info("Contact successful deleted");
		} catch (RuntimeException e) {
			log.info("...deleting contact failed", e);
			throw e;
		}
	}

}

