package com.bevy.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bevy.model.Address;
import com.bevy.repository.AddressRepository;

@Service
public class AddressService {
	
	private static final Log log = LogFactory
			.getLog(AddressService.class);
	
	@Autowired
	AddressRepository addressRepository;	
	
	
	public Optional<Address> getAddressByZipOrCity(String zip, String city) {		
		return addressRepository.findByZipOrCityIgnoreCase(zip, city);
	}

	@Transactional
	public Address getByCity(String city) throws IOException {
		log.info("...start GET Location by city instance");
		try {
			Optional<Address> location = addressRepository.findByCityIgnoreCase(city);
				if(location.isEmpty()) {
					log.error("Locations not found");				
				}else {
					log.info("GET All Locations");
				}	
			return location.get();
		} catch (RuntimeException e) {
			log.info("GET Locations failed", e);
			throw e;
		}
	}
	
	@Transactional
	public Address getByZip(String zip) throws IOException {
		log.info("...start GET Location by ZIP instance");
		try {
			Optional<Address> location = addressRepository.findByZip(zip);
				if(location.isEmpty()) {
					log.error("Location by ZIP not found");				
				}else {
					log.info("GET Location by ZIP");
				}	
			return location.get();
		} catch (RuntimeException e) {
			log.info("GET Location by ZIP failed", e);
			throw e;
		}
	}
	
	
	@Transactional
	public List<Address> getAllAddresses() throws IOException {
		log.info("...start GET Locations instances");
		try {
			List<Address> locations = addressRepository.findAll();	
				if(locations.isEmpty()) {
					log.error("Locations not found");				
				}else {
					log.info("GET All Locations");
				}
			return locations;
		} catch (RuntimeException e) {
			log.info("GET Locations failed", e);
			throw e;
		}
	}
	
	@Transactional
	public Address saveAddress(Address location) {		
		log.info("...start saving location instance");		
		try {		
			log.info("New location successful saved");
			return addressRepository.saveAndFlush(location);	
		} catch (RuntimeException e) {
			log.info("...saving location failed", e);
			throw e;
		}
	}
	
	@Transactional
	public void deleteAddress(Integer id) {		
		log.info("...start deleting location instance");		
		try {
			addressRepository.deleteById(id);		
			log.info("Location successful deleted");
		} catch (RuntimeException e) {
			log.info("...deleting location failed", e);
			throw e;
		}
	}

}
