package com.bevy.model;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="address")
public class Address implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String city;

	private String zip;

	public Address() {
	}
	
	public Address(int id, String city, String zip) {
		this.id = id;
		this.city = city;
		this.zip = zip;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String toJSONString() {
		return "{\"id\":" + id + 
				", \"city\":\"" + city + 
				"\", \"zip\":\"" + zip + 
				"\"}";
	}	

}