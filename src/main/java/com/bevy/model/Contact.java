package com.bevy.model;

import java.io.Serializable;
import javax.persistence.*;

import org.joda.time.LocalDate;

import java.util.Date;

@Entity
@Table(name="contact")
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String address;

	//@Temporal(TemporalType.DATE)
	private LocalDate dob;

	private String name;

	private String surname;

	public Contact() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LocalDate getDob() {
		return this.dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String toJSONString() {
		return "{ \"id:\"" + id + 
				", \"address\":" + address + 
				", \"dob\":\"" + dob + 
				"\", \"name\":\"" + name + 
				"\", \"surname\":\"" + surname + 
				"\"}";
	}

}