package com.bevy.util;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import org.apache.log4j.Logger;
import com.bevy.model.Address;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;


public class JsonUtil {	
	
	private static Logger LOG = Logger.getLogger(JsonUtil.class);
	
	public static Address getAddressFromJSON(String address) throws IOException, ParseException{

		ObjectMapper mapper = new ObjectMapper();	
		JsonNode jsonNode = mapper.readTree(address);
		
		Address a = new Address();
				a.setId(jsonNode.get("id").asInt());
				a.setCity(jsonNode.get("city").asText());;
				a.setZip(jsonNode.get("zip").asText());

		return a;	 
	}
	
	public static String getJsonFromObject(Object object, boolean prityPrint){
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS,false);
		//mapper.configure(Feature.FAIL_ON_EMPTY_BEANS, false);	
		if (prityPrint){
		//	mapper.enable(Feature.INDENT_OUTPUT);
			mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		}
		mapper.registerModule(new JodaModule());		
		
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			LOG.info(e.getMessage()+"; stack: "+Arrays.deepToString(e.getStackTrace()));
		} catch (JsonMappingException e) {
			LOG.info(e.getMessage()+"; stack: "+Arrays.deepToString(e.getStackTrace()));
		} catch (IOException e) {
			LOG.info(e.getMessage()+"; stack: "+Arrays.deepToString(e.getStackTrace()));
		}
		return null;
	}
	public static String getJsonFromObject(Object object){
		return getJsonFromObject(object, false);
	}

}
