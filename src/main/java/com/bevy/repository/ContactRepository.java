package com.bevy.repository;

import java.util.List;
import java.util.Optional;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.bevy.model.Contact;



@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer>{
	
	@Query(value = "SELECT * FROM contact " + 
			"WHERE json_unquote(address->'$.zip') = ?1 OR json_unquote(address->'$.city') = ?2" , nativeQuery = true)
	public List<Contact> findByZipOrCity(String zip, String city);
	
	List<Contact> findByNameOrSurnameIgnoreCase(String name, String surname);
	
	List<Contact> findByDob(LocalDate dob);
	
	Optional<Contact> findByNameAndSurnameIgnoreCase(String name, String surname);
	
	Boolean existsByNameAndSurnameIgnoreCase(String name, String surname);

}