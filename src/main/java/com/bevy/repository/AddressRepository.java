package com.bevy.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bevy.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{
	
	Optional<Address> findByZipOrCityIgnoreCase(String zip, String city);
	
	Optional<Address> findByCityIgnoreCase(String city);
	
	Optional<Address> findByZip(String zip);

}
