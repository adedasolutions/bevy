# BEVY - simple contact manager

This sample shows the basics CRUD operations with JPA

Java 11 is needed to run this sample

Clone
--------
```sh
git clone git@gitlab.com:adedasolutions/bevy.git
```

Switch to development branch.(On master branch is version witch doesn't use foreign key from Contact class to Address class but use JSON field, but it's intended for MySql as in-memory H2 database doesn't have support for JSON functions. This can gain in performance for some scenarios)

```sh
git checkout development
```
Run
--------
```sh
cd bevy
```
```sh
mvn package
```
```sh
java -jar target/bevy-0.0.1-SNAPSHOT.jar
```

Access
--------
##### Swagger (OpenAPI definition)
```
http://localhost:8080/swagger-ui.html
```
##### H2 console
```
http://localhost:8080/h2
```
##### JDBC URL
```
jdbc:h2:mem:bevy
```
##### User Name
```
sa
```
##### Web interface (Not finished)
```
http://localhost:8080
```
Testing
--------
If not using IDE (Eclipse), run all Unit tests with command from project root folder:
```sh
mvn -q test -Dtest=BevyApplicationTests
```
or just by method names:
```sh
mvn -q test -Dtest=#testCreateContact
```
```sh
mvn -q test -Dtest=#testGetAllContacts
```

Edit
--------

Eclipse 4.17.0

